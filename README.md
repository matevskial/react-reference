# React reference app

A project containing examples of react concepts, for learning purposes

## Some available Scripts

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

## Some tips for developing React apps

* Components:
    * can be nested
    * reusable
    * configurable(using props system)
    
* Hooks
    * examples are given in components/HooksExample.js
    
    * a way to make functional components use state
    
    * useEffect
        * configure a code to run when a component is rendered first time only
        * configure a code to run when a component is rendered first time and whenever it rerenders
        * configure a code t run when a component is rendered first time and whenever it rerenders and when some data has changed
        
    * useRef
        * used to get a reference to a DOM element
            ```javascript
          const ref = useRef();
            ```
    
* Don't put heavy computation, API calls, or things that update the state in the render method

* In a component, put helper methods, objects at the top and the component at the bottom.

* A controlled component knows and maintains the data that the html elements contain.

* In a functional component, place the methods specific to the component
inside the function(for example event handlers)

* Events
    * In React, events that are added manually with addEventListener are executed first(with the usual event bubbling propagated)
    and then the events added with react are executed(still with the usual event bubbling)
    * It is good practice to 