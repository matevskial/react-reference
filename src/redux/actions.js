const createSong = (payload) => {
    return {
        type: "CREATE_SONG",
        payload: payload
    };
}

const deleteSong = (name) => {
    return {
        type: "DELETE_SONG",
        payload: {
            name: name
        }
    };
}

const selectSong = (payload) => {
    return {
        type: "SELECT_SONG",
        payload: payload
    };
}

export {createSong, deleteSong, selectSong};