import {createStore} from 'redux';

import reducers from './reducers';

const songStore = createStore(reducers);

export default songStore;
