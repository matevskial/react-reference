// Some rules about reduces:
// reduces should not modify the first argument that represent the state directly
// reduces should create new state with updated data, and return that state
// reducers should always return a state(modified or not)

import {combineReducers} from "redux";

const defaultSongList = [
    {
        name: "Tangeirne Dream - Invisible limits",
        album: "Tangerine Dream - Stratosfear"
    },
    {
        name: "Tangeirne Dream - Cloudburst flights",
        album: "Tangerine Dream - Force Majeure"
    }
]

const songs = (oldListOfSongs = defaultSongList, action) => {
    if (action.type === "CREATE_SONG") {
        return [...oldListOfSongs, action.payload];
    }

    return oldListOfSongs;
}

const albums = (oldListOfAlbums = {}, action) => {
    if (action.type === "CREATE_SONG") {
        const song = action.payload;
        const oldAlbum = oldListOfAlbums[song.album] || [];
        let updatedAlbum = [...oldAlbum, song];
        return {...oldListOfAlbums, [song.album]: updatedAlbum}
    }

    return oldListOfAlbums;
}

const selectedSong = (oldSelectedSong = null, action) => {
    if(action.type === "SELECT_SONG") {
        return action.payload;
    }

    return oldSelectedSong;
}

// the names of the properties are the names of the state each reducer maintains
export default combineReducers({
    songs: songs,
    albums: albums,
    selectedSong: selectedSong
});