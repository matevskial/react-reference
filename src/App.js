import React, {Component} from "react";

import logo from './logo.svg';
import './App.css';

import ComponentExample from "./components/ComponentExample";
import FunctionalComponentExample from "./components/FunctionalComponentExamples";
import ControllerComponent from "./components/ControllerComponent";
import HooksExample from "./components/HooksExample";
import {Link} from "react-router-dom";

class App extends Component {
  constructor() {
    // this is where the state is initialized
    // you can also load some data here - but best practices say that you do data loading in other lifecycle methods

    super();

    this.state = {
      typeOfComponent: "functional",
      controllerStatus: true,
    }

    this.changeTypeOfComponent = this.changeTypeOfComponent.bind(this);
    this.showControllerComponent = this.showControllerComponent.bind(this);

    this.hideControllerComponent = () => this.setState({controllerStatus: false})

  }

  showControllerComponent() {
    this.setState({
      controllerStatus: true,
    })
  }

  componentDidMount() {
    // called after the component is mounted and ready and after the render lifecycle method
    // can be used for API calls, loading data from APIs, operating on DOM
    // can modify state with setState but it will cause another rendering
    document.getElementsByName("inputTextField")[0].value = "initial text";
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // this is not called after initial render
    // this is called after state/prop update
    // used for operating on DOM after update, network requests
    // can also use setState, but with care, since you can cause infinite loop

    console.log("it did update");
  }

  componentWillUnmount() {
    // used for clean up such as clearing timers, cancelling API calls, clearing cache in storage
    // cannot use setState
    document.getElementsByName("inputTextField")[0].value = "";
  }

  changeTypeOfComponent(e) {
    this.setState({
      typeOfComponent: e.target.value,
    });
  }

  render() {
    // this lifecycle method has to be pure
    return (
        <div className="app">
          <header className="app-header">
            <img src={logo} className="app-logo" alt="logo"/>
            <p>
              Edit <code>src/App.js</code> and save to reload.
            </p>
            <a
                className="app-link"
                href="https://reactjs.org"
                target="_blank"
                rel="noopener noreferrer"
            >
              Learn React
            </a>
            {/*<a href="/flexManipulator"><button>Go to flex manipulator</button></a>*/}
            <Link to="/flexManipulator"><button>Go to flex manipulator</button></Link>
            <Link to="/reduxClassComponentExample"><button>Go to redux with class component example</button></Link>
            <input type="text" name="inputTextField" />
            <button onClick={this.showControllerComponent} disabled={this.state.controllerStatus}>Show options</button>
            <button onClick={this.hideControllerComponent} disabled={!this.state.controllerStatus}>Hide options</button>
            { this.state.controllerStatus ? <ControllerComponent onChageHandler={this.changeTypeOfComponent}/> : null }
            <ComponentExample/>
            <FunctionalComponentExample typeOfComponent={this.state.typeOfComponent}/>
            <HooksExample />
          </header>
        </div>
    );
  }
}

export default App;
