import React from 'react';

import "../styles/Filterer.css";

function Filterer(props) {
    return (
        <div className="filterer">
            <label htmlFor="select-flex-flow">Choose a flex flow: </label>
            <select name="select-flex-flow" value={props.flexDirection} onChange={props.changeSelectHandler}>
                <option>
                    column
                </option>
                <option>
                    row
                </option>
            </select>
        </div>
    );
}

export default Filterer;
