import React, {Component} from 'react';
import {connect} from 'react-redux';
import '../styles/ReduxWithClassComponentExample.css'
import {selectSong} from '../redux/actions';

class ReduxWithClassComponentExample extends Component {

    constructor(props) {
        super(props);

        this.displaySelectedSong = this.displaySelectedSong.bind(this);
    }

    displaySelectedSong() {
        if(this.props.selectedSong !== null) {
            return (
                <div>
                    Selected song:
                    <p>{this.props.selectedSong.name}</p>
                </div>
            );
        }

        return null;
    }

    render() {
        return (
            <div className={"redux-with-class-component-example"}>
                List songs in redux class component example

                <ul>
                    {this.props.songs.map(song => {
                        return <li key={song.name} onClick={() => this.props.selectSong(song)}>{song.name}</li>
                    })}
                </ul>

                {this.displaySelectedSong()}
            </div>
        );
    }
}

// maps redux store state to props for this component
const mapStateToProps = (state) => {

    console.log("the state in mapStatateToProps:", state);

    return {songs: state.songs, selectedSong: state.selectedSong};
}

// notice we also pass the action creator selectSong
// connect gives us the option to automatically call the store dispatch, when the action creator is called
export default connect(mapStateToProps, {selectSong})(ReduxWithClassComponentExample);