const FunctionalComponentExample = (props) => (<div className="box1">
    <p>This is inside a {props.typeOfComponent} component</p>
</div>)

export default FunctionalComponentExample;