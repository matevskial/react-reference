import "../styles/Container.css";

function Container(props) {
    return (
        <div className="container" style={{flexDirection: props.flexDirection}}>
            <div className="container-div">Item 1</div>
            <div className="container-div">Item 2</div>
            <div className="container-div">Item 3</div>
        </div>
    );
}

export default Container;