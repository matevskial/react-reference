import React, { Component } from 'react'

class ComponentExample extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div className= "box">
                <h2>A heading in component</h2>
                <p>Some text</p>
            </div>
        );
    }

}

export default ComponentExample;