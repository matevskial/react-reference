import React, {useState, useEffect} from "react";

/**
 * Function component using hooks
 * Hooks are way to "hook" in to React features like state and lifecycle methods
 * To use state in function component, use the function useState
 * Use usEffect to implement the behavior of lifecycle methods like:
 *  componentDidMount
 *  componentDidUpdate
 *  componenWillUnmount
 *
 *  It is possible to create custom hooks to reuse stateful behavior
 * @returns {JSX.Element}
 * @constructor
 */
const HooksExample = () => {
    const [count, setCount] = useState(0);

    const incrementOnClick = () => {
        setCount(count + 1);
    }

    const resetCount = () => {
        setCount(0);
    }

    // Initial render only
    // cleanup method here will execute when the component is removed from DOM
    useEffect(() => {
        console.log("Initial render only")

        return () => {
            console.log("Cleanup done when this component is removed from DOM")
        }
    }, []);

    // Initial render and every rerender
    useEffect(() => {
        console.log("Initial render and every rerender")
    });

    // Initial render and every rerender if data has changed since last render
    useEffect(() => {
        document.getElementById("incrementButton").addEventListener("click", incrementOnClick);

        // useEffect can return one method that does cleanup before re-runing this useEffect
        return () => {
            console.log("Cleanup done before re-runing the method suplied to this useEffect");
        }
    }, [count])

    return (
        <div className="hooks-example">
            <p>A function component implementing counter with hooks</p>
            <p>Counter: {count}</p>
            <button id="incrementButton">Increment count</button>
            <button onClick={resetCount}>Reset count</button>
        </div>
    );
}

export default HooksExample;