import React from "react";

import '../styles/FlexManipulatorApp.css';
import Filterer from "./Filterer";
import Container from "./Container";

class FlexManipulatorApp extends React.Component {
    constructor() {
        super();

        this.state = {
            flexDirection: "column"
        };

        this.changeFlexDirection = this.changeFlexDirection.bind(this);
    }

    changeFlexDirection(e) {
        this.setState({
            flexDirection: e.target.value
        });
    }

    render() {
        return (
            <div className="flex-manipulator-app">
                <Filterer flexDirection={this.state.flexDirection} changeSelectHandler={this.changeFlexDirection} />
                <Container flexDirection={this.state.flexDirection} />
            </div>
        );
    }
}

export default FlexManipulatorApp;
