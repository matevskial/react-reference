function ControllerComponent(props) {
    return (
        <div className="controller-component">
            <label htmlFor="select-type-of-component">Choose a type of component: </label>
            <select name="select-type-of-component" value={props.typeOfComponent} onChange={props.onChageHandler}>
                <option>
                    class
                </option>
                <option>
                    functional
                </option>
            </select>
        </div>
    );
}

export default ControllerComponent;